package com.zhazha;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动类
 */
@SpringBootApplication
public class SpringCloudConfigClientApplication
{
    public static void main( String[] args )
    {
        SpringApplication.run(SpringCloudConfigClientApplication.class,args);
    }
}
