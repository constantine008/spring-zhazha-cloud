package com.zhazha.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName IndexController
 * @autor huangpu
 * @DATE 2019/12/16
 **/
@RestController
public class IndexController {

    @GetMapping("/helloworld")
    public String helloworld(){
        return "helloworld";
    }
}

    
    