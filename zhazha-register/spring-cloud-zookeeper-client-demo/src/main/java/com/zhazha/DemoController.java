package com.zhazha;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName DemoController
 * @autor huangpu
 * @DATE 2019/12/16
 **/
@RestController
public class DemoController {

    @Autowired
    HelloWorldClient helloWorldClient;

    @GetMapping("/greeting")
    public String greeting(){
        return helloWorldClient.helloWorld();
    }
}

    
    