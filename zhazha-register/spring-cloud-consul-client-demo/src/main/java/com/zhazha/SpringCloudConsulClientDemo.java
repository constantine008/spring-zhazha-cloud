package com.zhazha;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * Hello world!
 *@EnableDiscoveryClient 启用服务发现
 */
@EnableDiscoveryClient
@SpringBootApplication
public class SpringCloudConsulClientDemo
{


    public static void main( String[] args )
    {
        SpringApplication.run(SpringCloudConsulClientDemo.class,args);
    }


}
