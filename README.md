# spring-zhazha-cloud

#### 介绍



#### 软件架构

软件架构说明


| 模块名称 | 模块说明 |
| ---- | ---- |
|   zhazha-config   | 配置中心  |
| zhazha-gateway | 网关服务 |
| zhazha-load-balancing | 负载均衡模块 |
| zhazha-middle-ware | 中间件操作服务 |
| zhazha-register | 注册中心 |
| zhazha-utils | 全局工具类 |
|      |      |
|      |      |
|      |      |

框架基础
https://github.com/spring-projects/spring-cloud/wiki/Spring-Cloud-Finchley-Release-Notes

本系统基于spring-cloud Finchley.SR3 版本，当时版本受到限制

领域驱动模块

* 领域模块

在一个业务模块中，需要提炼出一个场景，就像拍电影一样，每一个业务模块都受到场景的约束，我们的每一个模块就像拍电影，有它的悲欢离合和业务约束。

组成场景的要素通常被称为6W模型，即描写场景需要who 、what、why 、where、when、how。

领域模块通常很多业务不属于本领域，需要引入限界上下文来解决这个问题。

领域划分谁 -> 浏览商品->下订单->支付

* 统一语言:

统一的领域描述

领域的行为描述

* 限界上下文

我们需要根据业务的相关性、耦合的强弱程度、分离的关注点对这些活动进行归类，找到不同类别之间存在的边界，就是限界上下文。

上下文是业务目标，限界则是保护和隔离上下文的边界，避免业务目标的不单一而带来的混乱和概念的不一致。

微服务

领域  

下订单-上下文 (限界)

数据库-> 服务

限界上下文 api  (划分)

商品 粒度

 

支付



#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)